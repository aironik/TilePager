//
//  ATPTilePagerView.h
//  TilePager
//
//  Created by Oleg Lobachev on 26/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ATPTilePagerViewDelegate;


/// @brief Контролл, реализующий отображение плиток с постраничным листанием.
@interface ATPTilePagerView : UIScrollView

@property (nonatomic, weak) IBOutlet id<ATPTilePagerViewDelegate> tilePagerDelegate;

@end
