//
//  ATPTilePagerView.m
//  TilePager
//
//  Created by Oleg Lobachev on 26/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

#import "ATPTilePagerView.h"

#import "ATPTilePagerViewDelegate.h"


static const CGFloat ATPTilePagerViewMargin = 10.f;
static const UIEdgeInsets ATPTilePagerViewPageInsets = { 16.f, 16.f, 16.f, 16.f };
static const NSUInteger ATPTilePagerTilesPerPage = 5;


@interface ATPTilePagerView()<UIScrollViewDelegate>

@property (nonatomic, assign, readonly) CGFloat margin;             // < Размер отступов между элементами на странице.
@property (nonatomic, assign, readonly) UIEdgeInsets pageInsets;    // < Отступы от краёв для страницы.

@property (nonatomic, assign, readonly) NSUInteger numberOfTiles;   // < Количество элементов. Получается из DataSource и кешируется.
@property (nonatomic, assign, readonly) NSUInteger numberOfPages;   // < Количество страниц. Вычисляется исходя из количества элементов.
@property (nonatomic, assign, readonly) CGSize pageSize;            // < Размер страницы. Вычисляется исходя из размера view
@property (nonatomic, assign) NSUInteger currentPage;               // < Номер текущей страницы.

@property (nonatomic, strong, readonly) NSMutableDictionary<NSNumber *, UIControl *> *loadedTiles;

@end


#pragma mark - Implementation

@implementation ATPTilePagerView


@synthesize numberOfTiles = _numberOfTiles;


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    _margin = ATPTilePagerViewMargin;
    _pageInsets = ATPTilePagerViewPageInsets;
    _loadedTiles = [@{ } mutableCopy];

    self.pagingEnabled = YES;
    self.delegate = self;
}

- (void)setTilePagerDelegate:(id)tilePagerDatasource
{
    _tilePagerDelegate = tilePagerDatasource;
    [self reload];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self reload];
}

- (void)didMoveToWindow
{
    [super didMoveToWindow];
    [self reload];
}


+ (BOOL)requiresConstraintBasedLayout
{
    return NO;
}

- (void)reload
{
    if (self.tilePagerDelegate != nil && !CGSizeEqualToSize(self.frame.size, CGSizeZero) && self.window != nil)
    {
        [self unload];
        [self load];
    }
}

- (void)load
{
    NSUInteger numberOfPages = [self numberOfPages];
    if (numberOfPages == 0)
    {
        self.contentSize = CGSizeZero;
        self.contentOffset = CGPointZero;
    }
    else
    {
        CGSize pageSize = self.pageSize;

        self.contentSize = CGSizeMake(pageSize.width, pageSize.height * numberOfPages);
        if (self.contentOffset.y + pageSize.height > self.contentSize.height)
        {
            [self setContentOffset:CGPointMake(0.f, pageSize.height * (numberOfPages - 1)) animated:YES];
        }
        [self loadActualPages];
    }
}

- (void)loadActualPages
{
    const NSUInteger currentPage = self.currentPage;
    [self loadPage:currentPage - 1];
    [self loadPage:currentPage];
    [self loadPage:currentPage + 1];
}

- (NSUInteger)numberOfTiles
{
    if (_numberOfTiles == 0)
    {
        _numberOfTiles = [self.tilePagerDelegate numberOfTilesInTilePager:self];
    }
    return _numberOfTiles;
}

- (void)unloadNnumberOfTiles
{
    _numberOfTiles = 0;
}

- (NSUInteger)numberOfPages
{
    const NSUInteger numberOfPages = self.numberOfTiles / ATPTilePagerTilesPerPage
            + (self.numberOfTiles % ATPTilePagerTilesPerPage > 1 ? 1 : 0);
    return numberOfPages;
}

- (void)loadPage:(NSUInteger)pageIndex
{
    const BOOL pageExists = (pageIndex >= 0 && pageIndex < self.numberOfPages);
    if (pageExists)
    {
        for (NSUInteger idx = 0; idx < ATPTilePagerTilesPerPage; ++idx)
        {
            [self loadTileAtIndexOnPage:idx pageIndex:pageIndex];
        }
    }
}

- (void)loadTileAtIndexOnPage:(NSUInteger)indexOnPage pageIndex:(NSUInteger)pageIndex
{
    const NSUInteger tileIndex = pageIndex * ATPTilePagerTilesPerPage + indexOnPage;
    const CGRect tileOnPageFrame = [self frameForTileAtIndexOnPage:indexOnPage];
    const CGRect tileFrame = CGRectOffset(tileOnPageFrame, 0.f, pageIndex * self.pageSize.height);
    UIControl *tile = self.loadedTiles[@(tileIndex)];
    if (tile == nil)
    {
        tile = [self.tilePagerDelegate tilePager:self tileAtIndex:tileIndex withSize:tileFrame.size];
        if (tile != nil)
        {
            self.loadedTiles[@(tileIndex)] = tile;
            [tile addTarget:self action:@selector(tileTapped:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    tile.frame = tileFrame;
    [self addSubview:tile];
}

- (void)unload
{
    [self unloadNnumberOfTiles];
    for (UIControl *tile in self.loadedTiles.objectEnumerator)
    {
        [tile removeFromSuperview];
    }
    [self.loadedTiles removeAllObjects];
}

- (CGSize)pageSize
{
    const CGSize pageSize = self.frame.size;
    return pageSize;
}

- (NSUInteger)calculateCurrentPage
{
    const CGFloat pageHeight = self.pageSize.height;
    NSUInteger pageIndex = floorf((self.contentOffset.y + pageHeight / 2.f) /pageHeight);
    return pageIndex;
}

- (CGRect)frameForTileAtIndexOnPage:(NSUInteger)indexOnPage
{
    NSAssert(indexOnPage < ATPTilePagerTilesPerPage, @"Only 5 items can be on the page.");
    CGRect frame = CGRectZero;
    const CGRect contentFrame = UIEdgeInsetsInsetRect(CGRectMake(0.f, 0.f, self.pageSize.width, self.pageSize.height), self.pageInsets);

    const CGFloat heights[3] = { 0.35f, 0.3f, 0.35f };      // высоты строк
    const CGFloat widths0[2] = { 0.3f, 0.7f };              // ширины элементов в пераой строке
    const CGFloat widths2[2] = { 0.7f, 0.3f };              // ширины элементов в пераой строке

    CGRect frame0;
    frame0.origin = contentFrame.origin;
    frame0.size.width = roundf((contentFrame.size.width - self.margin) * widths0[0]);
    frame0.size.height = roundf((contentFrame.size.height - 2 * self.margin) * heights[0]);

    CGRect frame1;
    frame1.origin.x = CGRectGetMaxX(frame0) + self.margin;
    frame1.origin.y = frame0.origin.y;
    frame1.size.width = contentFrame.size.width - self.margin - frame0.size.width;
    frame1.size.height = frame0.size.height;

    CGRect frame2;
    frame2.origin.x = contentFrame.origin.x;
    frame2.origin.y = CGRectGetMaxY(frame0) + self.margin;
    frame2.size.width = contentFrame.size.width;
    frame2.size.height = roundf((contentFrame.size.height - 2 * self.margin) * heights[1]);

    CGRect frame3;
    frame3.origin.x = contentFrame.origin.x;
    frame3.origin.y = CGRectGetMaxY(frame2) + self.margin;
    frame3.size.width = roundf((contentFrame.size.width - self.margin) * widths2[0]);
    frame3.size.height = (contentFrame.size.height - 2 * self.margin - frame2.size.height - frame0.size.height);

    CGRect frame4;
    frame4.origin.x = CGRectGetMaxX(frame3) + self.margin;
    frame4.origin.y = frame3.origin.y;
    frame4.size.width = contentFrame.size.width - self.margin - frame3.size.width;
    frame4.size.height = frame3.size.height;

    switch (indexOnPage)
    {
        case 0:
            return frame0;
        case 1:
            return frame1;
        case 2:
            return frame2;
        case 3:
            return frame3;
        case 4:
            return frame4;
    }
    return frame;
}


#pragma mark - UIScrollView

- (void)setContentOffset:(CGPoint)contentOffset
{
    [super setContentOffset:contentOffset];

    const CGFloat maxDelta = 50.f;
    const CGFloat delta = MIN(maxDelta, ABS(contentOffset.y - self.pageSize.height * self.currentPage)) / maxDelta;
    const CGFloat angle = -delta * (M_PI / 12);

    const CGFloat centerX = CGRectGetMidX(self.bounds);

    for (UIControl *tile in self.loadedTiles.objectEnumerator)
    {
        const CGFloat tileCenterOffsetX = CGRectGetMidX(tile.frame) - centerX;
        CATransform3D transform = CATransform3DIdentity;
        transform.m34 = 0.0015f;
        transform.m21 = -tileCenterOffsetX / CGRectGetWidth(self.bounds) * sinf(angle) / 2.f;

        transform = CATransform3DRotate(transform, angle, 1.f, 0.f, 0.f);

        tile.layer.transform = transform;
    }
    if ([self calculateCurrentPage] != self.currentPage)
    {
        self.currentPage = [self calculateCurrentPage];
        [self loadActualPages];
    }
}


- (IBAction)tileTapped:(UIControl *)sender
{
    for (NSNumber *indexNumber in self.loadedTiles)
    {
        if (sender == self.loadedTiles[indexNumber])
        {
            [self.tilePagerDelegate tilePager:self didSelectTileAtIndex:[indexNumber unsignedIntegerValue]];
            return;
        }
    }
}


@end
