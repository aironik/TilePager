//
//  ATPTilePagerViewDelegate.h
//  TilePager
//
//  Created by Oleg Lobachev on 26/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

@class ATPTilePagerView;


@protocol ATPTilePagerViewDelegate<NSObject>

@required

/// @brief Количество элементов (плиток) для отолажения.
- (NSUInteger)numberOfTilesInTilePager:(ATPTilePagerView *)tilePager;

/// @brief Запросить элемент (плитку) с размерами по индексу.
- (UIControl *)tilePager:(ATPTilePagerView *)tilePager tileAtIndex:(NSUInteger)rowIndex withSize:(CGSize)size;

/// @brief Обработать нажатие на плитку с индексом.
- (void)tilePager:(ATPTilePagerView *)tilePager didSelectTileAtIndex:(NSUInteger)tileIndex;

@end
