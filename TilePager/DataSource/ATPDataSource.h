//
//  ATPDataSource.h
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

@class ATPCancallationToken;


/// @brief Источник данных для контролла ATPTilePager.
@interface ATPDataSource : NSObject

/// @brief Количество элементов двнных.
@property (nonatomic, assign, readonly) NSInteger numberOfItems;

/// @brief Асинхронно порлучить закешированную картинку по ключу.
/// @details Загружает картинку по ключу. Если картинка находится в кеше, то блок handle
///     вызывается синхронно. Иначе, загрузка картинки в кеш ставится в фоновую очередь и
///     блок handler вызывается асинхронно на очереди вызова метода. Если в кеше картинки
///     нет, то для загрузки вызывается  блок loader.

- (ATPCancallationToken *)imageForKey:(NSString *)key
                                 size:(CGSize)size
                               loader:(UIImage *(^)(void))loader
                              handler:(void(^)(UIImage *))handle;

@end
