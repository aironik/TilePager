//
//  ATPCancallationToken.h
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//


/// @brief Токен для отмены операции
@interface ATPCancallationToken : NSObject


@property (nonatomic, assign, readonly, getter=isCancelled) BOOL cancelled;

- (void)cancel;


@end
