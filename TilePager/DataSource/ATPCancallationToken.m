//
//  ATPCancallationToken.m
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

#import "ATPCancallationToken.h"


@implementation ATPCancallationToken


- (void)cancel
{
    _cancelled = YES;
}


@end
