//
//  ATPDataSource.m
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 aironik. All rights reserved.
//

#import "ATPDataSource.h"

#import "ATPCancallationToken.h"


static NSString *ATPDataSourceCacheDir = @"image_cache";              // < Имя каталога с кешем
static NSUInteger ATPDataSourceCacheSize = 30;                        // < Размер кеша


@interface ATPDataSource ()

@property (nonatomic, strong, readonly) NSString *cachePath;
@property (nonatomic, strong, readonly) NSFileManager *fileManager;
@property (nonatomic, strong, readonly) NSOperationQueue *operationQueue;

@property (nonatomic, strong, readonly) NSCache *cache;

@end


#pragma mark - Implementation

@implementation ATPDataSource


- (instancetype)init
{
    if (self = [super init])
    {
        _fileManager = [NSFileManager defaultManager];
        // Для изменяемых данных можно использовать NSCachesDirectory
        NSArray<NSURL *> *urls = [_fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        if (urls.count == 0)
        {
            // Всё совсем-совсем плохо.
        }
        _cachePath = [urls.firstObject URLByAppendingPathComponent:ATPDataSourceCacheDir].path;
        if (![_fileManager createDirectoryAtPath:_cachePath withIntermediateDirectories:YES attributes:nil error:NULL])
        {
            // Всё совсем-совсем плохо.
        }
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;

        _cache = [[NSCache alloc] init];
        _cache.countLimit = ATPDataSourceCacheSize;
    }
    return self;
}

- (NSString *)cachedPathForKey:(NSString *)key
{
    return [self.cachePath stringByAppendingPathComponent:key];
}

- (void)addImage:(UIImage *)image forKey:(NSString *)key withSize:(CGSize)size handler:(void (^)(UIImage *))handler
{
    NSString *path = [self cachedPathForKey:key];
    [self.operationQueue addOperationWithBlock:^
    {
        [self resizeImage:image size:size handler:^(UIImage *scaledImage)
        {
            NSData *imageData = UIImagePNGRepresentation(scaledImage);
            [imageData writeToFile:path atomically:YES];
            [self.cache setObject:scaledImage forKey:key];
            handler(scaledImage);
        }];
    }];
}

- (void)resizeImage:(UIImage *)image size:(CGSize)size handler:(void (^)(UIImage *))handler
{
    UIGraphicsBeginImageContextWithOptions(size, YES, UIScreen.mainScreen.scale);
    const CGSize imageSize = image.size;
    const CGFloat aspect = (imageSize.width / imageSize.height);
    CGSize scaledSize;
    if (imageSize.width / size.width > imageSize.height / size.height)
    {
        scaledSize = CGSizeMake(size.height * aspect, size.height);
    }
    else
    {
        scaledSize = CGSizeMake(size.width, size.width / aspect);
    }
    [image drawInRect:CGRectMake((size.width - scaledSize.width) / 2.f, (size.height - scaledSize.height) / 2.f,
            scaledSize.width, scaledSize.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    handler(scaledImage);
}

- (ATPCancallationToken *)imageForKey:(NSString *)originalKey
                                 size:(CGSize)size
                               loader:(UIImage *(^)(void))loader
                              handler:(void(^)(UIImage *))handler
{
    NSString *cacheKey = [NSString stringWithFormat:@"%@_%.0f_%.0f", originalKey, size.width, size.height];
    UIImage *image = [self.cache objectForKey:cacheKey];
    ATPCancallationToken *token = nil;
    if (image != nil)
    {
        handler(image);
    }
    else
    {
        dispatch_queue_t handlerQueue = dispatch_get_current_queue();
        token = [[ATPCancallationToken alloc] init];
        __weak typeof(self) weakSelf = self;
        [self.operationQueue addOperationWithBlock:^
        {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if (!token.cancelled)
            {
                NSString *path = [strongSelf cachedPathForKey:cacheKey];
                UIImage *image = [UIImage imageWithContentsOfFile:path];
                if (image == nil)
                {
                    image = loader();
                }
                if (image != nil)
                {
                    [self addImage:image forKey:cacheKey withSize:size handler:^(UIImage *image)
                    {
                        dispatch_async(handlerQueue, ^
                        {
                            if (!token.cancelled)
                            {
                                handler(image);
                            }
                        });
                    }];
                }
            }
        }];
    }

    return token;
}

@end
