//
//  AppDelegate.h
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 Oleg Lobachev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

