//
//  ATPViewController.m
//  TilePager
//
//  Created by Oleg Lobachev on 25/11/2017.
//  Copyright © 2017 Oleg Lobachev. All rights reserved.
//

#import "ATPViewController.h"

#import "ATPDataSource.h"
#import "ATPTilePagerView.h"
#import "ATPTilePagerViewDelegate.h"


@interface ATPViewController ()<ATPTilePagerViewDelegate>

@property (nonatomic, strong, readonly) ATPTilePagerView *tilePagerView;
@property (nonatomic, strong) ATPDataSource *imagesDataSource;

@end


#pragma mark - Implementation

@implementation ATPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.imagesDataSource = [[ATPDataSource alloc] init];
}


#pragma mark - ATPTilePagerViewDelegate Implementation

- (NSUInteger)numberOfTilesInTilePager:(ATPTilePagerView *)tilePager
{
    return 15;
}

- (UIControl *)tilePager:(ATPTilePagerView *)tilePager tileAtIndex:(NSUInteger)tileIndex withSize:(CGSize)size
{
    UIControl *tile = [[UIControl alloc] initWithFrame:CGRectMake(0.f, 0.f, size.width, size.height)];

    NSString *imageName = [NSString stringWithFormat:@"img%lu", (unsigned long)tileIndex];

    ATPCancallationToken *token = [self.imagesDataSource imageForKey:imageName
                                                                size:size
                                                              loader:^UIImage *()
    {
        // +imageNamed кеширует загруженные данные картинки. Нам этого не нужно. Поэтому, мы используем другой способ загрузки.
        NSString *originalFileName = [[NSBundle bundleForClass:self.class] pathForResource:imageName ofType:@"jpg"];
        UIImage *originalImage = [UIImage imageWithContentsOfFile:originalFileName];
        return originalImage;
    }
                                                             handler:^(UIImage *image)
    {
        [tile addSubview:[[UIImageView alloc] initWithImage:image]];
    }];

    return tile;
}

- (void)tilePager:(ATPTilePagerView *)tilePager didSelectTileAtIndex:(NSUInteger)tileIndex
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Item Index", @"")
                                                                   message:[NSString stringWithFormat:@"%@", @(tileIndex)]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {}]];
    [self presentViewController:alert animated:YES completion:nil];

}


@end
